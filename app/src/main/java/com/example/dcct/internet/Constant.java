package com.example.dcct.internet;

public class Constant {
    //虚拟机ip：10.0.2.2
    public static final String BASE_URL = "http://47.102.206.167:8080";
    public static final String LOGIN_API = "/login";
    public static final String REGISTER_API = "/register";
    public static final String COVER_API ="/getAllCover";
    public static final String QUERY_API = "/query";
    public static final String RECORD_API = "/record/{uid}";
    public static final String SIGN_OUT = "/sign_out/{uid}";
}
