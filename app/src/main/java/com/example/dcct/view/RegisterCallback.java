package com.example.dcct.view;

import com.example.dcct.base.BaseCallback;
import com.example.dcct.bean.BackResultData;

public interface RegisterCallback extends BaseCallback {

    void onLoadRegisterData(BackResultData backData);

}
